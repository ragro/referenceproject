export class ArrayUtils {

    public arrayFilter(array, element) {
        return array.filter(e => e !== element);
    }

    public arrayObjectFilter(array, element) {
        return array.filter(e => e._id !== element._id);

    }

}
