import { HttpHeaders } from '@angular/common/http';

export class Constants {
   
    //USER CNSTANTS
    public static get TOKEN(): string {
        return 'token'
    }
    public static get USER(): string {
        return 'user'
    }
    public static get MESSAGE(): string {
        return 'msg'
    }
    public static get EXPIRED(): string {
        return 'expired'
    }
    public static get ID_TOKEN():string{
        return 'id_token'
    }

    //TEST
    public static get TEST(): string {
        return 'test'
    }
    public static get QUESTIONS(): string {
        return 'questions'
    }
    public static get SELECTED_QUESTIONS(): string {
        return 'selectedQuestions'
    }
    public static get UNSELECTED_QUESTIONS_COUNT(): string {
        return 'unselectedQuestionCount'
    }
    public static get SELECTED_QUESTIONS_COUNT(): string {
        return 'selectedQuestionCount'
    }
    public static get CHECKED_ARRAY(): string {
        return 'checkedArray'
    }
    
    //PDF CONSTANTS
    public static get PDF(): string {
        return 'pdf'
    }
    public static get SAMPLE_PDF(): string {
        return 'samplepdf'
    }
    //VALIDATION COONSTANTS
    public static get EMAIL_VALIDATION(): string{
        return '[a-zA-Z0-9._]+[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}'
    }
}