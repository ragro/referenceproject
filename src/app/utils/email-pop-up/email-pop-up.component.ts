import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-email-pop-up',
  templateUrl: './email-pop-up.component.html',
  styleUrls: ['./email-pop-up.component.css']
})
export class EmailPopUpComponent implements OnInit {

  constructor(private AuthService:AuthService) { }

  ngOnInit() {
  }
  submitEmail(f){
    // console.log(f.value);
    this.AuthService.emailPopUp.close(f.value);
  }

}
