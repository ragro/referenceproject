export class ErrorMessageUtils{
    public static get NOT_RESPONDING():string{
        return 'Server Not responding!!'
    }
    public static get SOMETHING_WENT_WRONG():string{
        return 'Something went wrong!!'
    }
    public static get SERVER_ERROR():string{
        return 'Server Error!!'
    }
    
}