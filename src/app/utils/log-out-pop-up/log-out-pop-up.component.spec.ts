import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogOutPopUpComponent } from './log-out-pop-up.component';

describe('LogOutPopUpComponent', () => {
  let component: LogOutPopUpComponent;
  let fixture: ComponentFixture<LogOutPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogOutPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogOutPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
