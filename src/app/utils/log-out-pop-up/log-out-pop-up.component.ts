import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';


@Component({
  selector: 'app-log-out-pop-up',
  templateUrl: './log-out-pop-up.component.html',
  styleUrls: ['./log-out-pop-up.component.css']
})
export class LogOutPopUpComponent implements OnInit {

  constructor(public AuthService:AuthService,
              public router :Router 
  ){}
  ngOnInit()
  {}

  logout(){
    this.AuthService.logout();
    this.router.navigate(['/']);    
    this.AuthService.logOutPopUp.close();    
  }
}
