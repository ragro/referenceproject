import { HttpHeaders } from "@angular/common/http";

export class HeaderUtils {
    public static get headersWithAuthorisation() {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type','application/json');
        headers = headers.set( 'Authorization', 'JWT ' + localStorage.getItem('id_token'))
        return headers;         
    }
    public static get headers() {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type','application/json');
        return headers;         
    }
}