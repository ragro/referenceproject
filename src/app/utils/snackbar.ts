import { MatSnackBar, MatSnackBarConfig } from "../../../node_modules/@angular/material";
import { Injectable } from "../../../node_modules/@angular/core";

@Injectable()
export class SnackBar {
    constructor(private snackBar: MatSnackBar) { }

    public openSnackBar(message: string, className: string) {
        let config = new MatSnackBarConfig()
        config.duration = 2000;
        config.panelClass = className;
        config.verticalPosition = 'top';
        console.log(config);
        this.snackBar.open(message,'',config);
    }
    
}




  


