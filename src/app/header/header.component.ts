import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LogOutPopUpComponent } from '../utils/log-out-pop-up/log-out-pop-up.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private AuthService: AuthService,
    public router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
  }

  dashboard() {
    this.router.navigate(['/dashboard']);
  }

  logout(){
    this.AuthService.logOutPopUp = this.dialog.open(LogOutPopUpComponent);
    this.AuthService.logOutPopUp.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}

