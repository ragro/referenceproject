import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { QuestionsService } from '../services/questions.service';
import { MatDialog } from '@angular/material';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { TestDetailComponent } from '../test-detail/test-detail.component';
import { Constants } from '../utils/constants';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  pdfArray: any[] = [];
  samplepdfArray: any[] = [];
  cardDisplay = false;
  show = false;
  dialogRef;
  flag = false;
  constructor(public router: Router,
    private authService: AuthService,
    public questionService: QuestionsService,
    private dialog: MatDialog) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }


  // OnInit
  ngOnInit() {
    this.questionService.getPDFs().subscribe(
      (res) => {
        if (res[Constants.PDF].length > 0) {
          this.cardDisplay = true;
        }
        this.pdfArray = res[Constants.PDF];
        this.samplepdfArray = res[Constants.SAMPLE_PDF];
      }
    );
    this.questionService.selectedQuestions = [];
    this.questionService.unselectedQuestions = [];
    this.localStorageClear();
  }


  // Create New Test
  newTest() {
    this.questionService.show = true;
    this.flag = true;
    this.questionService.editMode = false;
  }

  //   Download created PDF
  downloadPDF(i) {
    const downloadPDF = JSON.parse(this.pdfArray[i].content);
    pdfMake.createPdf(downloadPDF.docDefinition).download();
  }


  //   Download Sample PDF
  downloadSamplePDF(j) {
    const downloadSamplePDF = JSON.parse(this.samplepdfArray[j].content);
    pdfMake.createPdf(downloadSamplePDF.docDefinition).download();
  }


  //  Logout
  logout() {
    this.show = true;
  }


  // Logout confirmation
  onYes() {
    this.authService.logout();
    this.localStorageClear();
    this.router.navigate(['/']);
    return false;
  }

  onNo() {
    this.show = false;
  }

  //  Clear localstorage of testdetails
  localStorageClear() {
    localStorage.removeItem(Constants.TEST);
  }

  // close Test Details
  closeTestDetails(e: any) {
    if (this.flag) {
      this.flag = false;
    } else {
      if (e.target.closest('.modal-dialog.cascading-modal')) {
      } else {
        this.questionService.show = false;
        this.flag = false;
      }
    }
  }
  openTestDetailDialogbox() {
    this.questionService.testDetailDialogBox = this.dialog.open(TestDetailComponent, { width: '900px', disableClose: true }  );
    this.questionService.testDetailDialogBox.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
