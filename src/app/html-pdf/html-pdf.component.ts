import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { QuestionsService } from '../services/questions.service';
import { AuthService } from '../services/auth.service';
import { EmailPopUpComponent } from '../utils/email-pop-up/email-pop-up.component';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import * as jspdf from 'jspdf';
import * as html2canvas from 'html2canvas';
import { Constants } from '../utils/constants';
import { SnackBar } from '../utils/snackbar';
// import { NgFlashMessageService } from '../../../node_modules/ng-flash-messages';


@Component({
  selector: 'app-html-pdf',
  templateUrl: './html-pdf.component.html',
  styleUrls: ['./html-pdf.component.css']
})
export class HtmlPdfComponent implements OnInit, AfterViewInit {

  doc: any;
  testDetails: any;
  test: any;
  arr = [];
  show: boolean = false;
  pdfLoader: boolean = false;
  content = [];


  @ViewChild('topdf') pdfcreate: ElementRef;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private questionService: QuestionsService,
    private authService: AuthService,
    private snackbar:SnackBar,
    // private ngFlashMessageService:NgFlashMessageService,
    public dialog: MatDialog) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  questions: any[] = [];
  margins = {
    top: 70,
    bottom: 40,
    left: 30,
    width: 550
  };
  text = '';
  imageArray = [];

  enterEmailShow = false;
  emailData = {
    emails: '',
    message: ''
  };
  htmlCanvasArray = [];


  ngOnInit() {
    // function toDataURL(url, callback) {
    //   const xhr = new XMLHttpRequest();
    //   xhr.onload = function () {
    //     const reader = new FileReader();
    //     reader.onloadend = function () {
    //       callback(reader.result);
    //     };
    //     reader.readAsDataURL(xhr.response);
    //   };
    //   xhr.open('GET', url);
    //   xhr.responseType = 'blob';
    //   xhr.send();
    // }
    // toDataURL('https://www.google.de/images/srpr/logo11w.png', function (dataUrl) {
    //   console.log('RESULT IS:', dataUrl);
    // });

    this.test = localStorage.getItem(Constants.TEST);
    const testInfo = JSON.parse(this.test);
    this.testDetails = {
      testName: testInfo.testName,
      oName: testInfo.oName,
      class: testInfo.class,
      acaYear: testInfo.startYear + '-' + testInfo.endYear,
      subject: testInfo.subject,
      examTime: testInfo.examTime,
      totalMarks: testInfo.totalMarks
    };
    this.questions = this.questionService.selectedQuestions;
    // if (this.questionService.questionType === 'image') {
    //   this.areThereImages = true;
    // } else {
    //   this.areThereImages = false;
    //   this.pdfTemplate();
    // }
  }

  ngAfterViewInit() {
    // for(var i=0;i<this.questions.length;i++){
    //   // var height = document.getElementById(JSON.stringify(i)).clientHeight;
    //   html2canvas(document.getElementById(JSON.stringify(i)),{async:false}).then(function(canvas:HTMLCanvasElement){
    //     var imgData = canvas.toDataURL("image/png");
    //     this.htmlCanvasArray[i] = imgData;
    //   });
    // };
  }

  // temp(){
    // console.log(this.htmlCanvasArray);
    
    // var position = 0;
    // var pdf = new jspdf('p','pt','a4');
    // for(var i=0;i<this.htmlCanvasArray.length;i++){
    //   console.log(i);
    //   pdf.addImage(this.htmlCanvasArray[i],'png',100,200*(i+1));
    // }
    // console.log("loop complete");
    
    // pdf.save();


     async generatePdf() {
      this.pdfLoader=true;  
      console.log(this.pdfLoader)
      var heightLeft = 841.89;
      const doc = new jspdf('p', 'pt', 'a4');
      const options = {
        pagesplit: true
      };
      var position = 0;

      // // const ids = document.querySelectorAll('[id]');
      const length = this.questions.length;
      // const id = document.getElementById("html-2-pdfwrapper");
      // doc.addHTML(id,0,0,options,function(){
      //   doc.save();
      // })
      var details = document.getElementById('test-details');
      var detailsHeight = document.getElementById('test-details').clientHeight;
      detailsHeight = (detailsHeight*72)/96
      heightLeft -= detailsHeight ;
      position = detailsHeight;

      html2canvas(details).then(function(canvas){
        doc.addImage(canvas.toDataURL('image/png'),'JPEG',0,0);
      })

      for (let i = 0; i < length; i++) {
        var chart = document.getElementById(JSON.stringify(i));
        var height = document.getElementById(JSON.stringify(i)).clientHeight;
        console.log("dsjdn");
        
        height = (height*72)/96;
        if(height>heightLeft){
          console.log("new page");
          heightLeft = 822;  
          position = 20;
          doc.addPage();   
        } 
          // excute this function then exit loop
          await html2canvas(chart, { scale: 1 }).then(function (canvas) { 
            doc.addImage(canvas.toDataURL('image/png'), 'JPEG',10,position );
            console.log(heightLeft);
            console.log(position);
            heightLeft -= height;
            position += height+10;
            heightLeft -= 10;
          });

  
      }
      // download the pdf with all charts
      // console.log(doc.output('datauristring'));
      this.router.navigate(['/dashboard']);    
      const promise = new Promise((resolve, reject) => {
        doc.save(this.testDetails.testName + '.pdf');  
        localStorage.setItem(Constants.SELECTED_QUESTIONS,"[]");
        localStorage.setItem(Constants.SELECTED_QUESTIONS_COUNT,'0');
        this.pdfLoader=false;
        resolve();
      }).then(() => {
        console.log(doc);        
      });
    }
    // download the pdf with all charts
    // doc.save(this.testDetails.testName + '.pdf');
  


  // download the pdf with all charts
  // doc.save('All_charts_' + Date.now() + '.pdf');

  // console.log("abc");
  // var position = 0;
  // var pdf = new jspdf('p','pt','a4');
  // var heightLeft = 1122.519685;
  // var promise = new Promise((resolve,reject)=>{
  //   for(var i=0;i<this.questions.length;i++) {
  //     console.log(i);
  //     var offsetHeight = document.getElementById(JSON.stringify(i)).clientHeight;
  //       if(offsetHeight > heightLeft){
  //         console.log("New Page");
  //         heightLeft = 1122.519685;
  //         pdf.addPage();
  //       } else {
  //         console.log(document.getElementById(JSON.stringify(i)));
  //         var promise = new Promise((resolve,reject)=> {


  //         html2canvas(document.getElementById(JSON.stringify(i)),{async:false}).then(function (canvas: HTMLCanvasElement) {
  //           console.log("no new page");
  //           var imgData = canvas.toDataURL("image/png");
  //           console.log(imgData);
  //           pdf.addImage(imgData, 'PNG', 100, position);
  //           // pdf.save();
  //           position += offsetHeight;
  //           heightLeft = heightLeft - offsetHeight;
  //           console.log(heightLeft);
  //           resolve();
  //         })

  //       })
  //         //   heightLeft = heightLeft - offsetHeight;
  //         // console.log(heightLeft);

  //       }

  //   }
  //   resolve();
  //   // return "completed";

  // })
  // .then(()=>{
  //   pdf.save();
  //   console.log("+++++++++++++++++++++++++++++++++++++++++++++");

  //   console.log("Completed in .then");
  //   console.log("+++++++++++++++++++++++++++++++++++++++++++++");


  // })
  // console.log("+++++++++++++++++++++++++++++++++++++++++++++");

  // console.log("completed out of promise");
  // console.log("+++++++++++++++++++++++++++++++++++++++++++++");


  // }



  //  LogOut

  logout() {
    this.show = true;
  }

  onYes() {
    this.authService.logout();
    localStorage.removeItem(Constants.USER);
    localStorage.removeItem(Constants.TEST);
    this.router.navigate(['/']);
    return false;
  }

  onNo() {
    this.show = false;
  }

  // Generate pdf

  // generate() {
  //   var dd = {
  //     content: this.arr,
  //     styles: {
  //       tableExample: {
  //         margin: [0, 5, 0, 15]
  //       },
  //       instituteStyle: {
  //         alignment: 'center',
  //         fontSize: 24,
  //         bold: true
  //       },
  //       header: {
  //         fontSize: 14,
  //         alignment: 'center'
  //       }
  //     }
  //   }
  //   var savepdf = pdfMake.createPdf(dd);
  //   pdfMake.createPdf(dd).download();
  //   this.questionService.savePDF(savepdf).subscribe(
  //     (res) => {
  //       this.router.navigate(['/dashboard']);
  //       this.localStorageClear();
  //     });
  // }

  localStorageClear() {
    localStorage.removeItem(Constants.TEST);
  }

  email() {
    this.enterEmailShow = true;
    this.authService.emailPopUp = this.dialog.open(EmailPopUpComponent);
    this.authService.emailPopUp.afterClosed().subscribe(result => {
      console.log('Dialog result:', result);
      if(result != undefined){
        this.emailData = result;
        this.onEmailSubmit();
      }
    });
  }


  // ===============================
  //      HTML To Canvas
  //===============================
  // captureHtmlToPdf() {
  //   var data = document.getElementById('exportthis');
  //   html2canvas(data).then(canvas => {
  //     // Few necessary setting options  
  //     var imgWidth = 208;
  //     var pageHeight = 295;
  //     var imgHeight = canvas.height * imgWidth / canvas.width;
  //     var heightLeft = imgHeight;

  //     const contentDataURL = canvas.toDataURL('image/png')
  //     let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
  //     var position = 0;
  //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
  //     pdf.save('MYPdf.pdf'); // Generated PDF   
  //   });
  // }


  //===============================
  //      Creating PDF
  //===============================
  // createPdf() {

  //   let pdfData = this.content;

  //   var docDefinition = {
  //     content: this.content,
  //     styles: {
  //       header: {
  //         fontSize: 18,
  //         bold: true
  //       },
  //       bigger: {
  //         fontSize: 15,
  //         italics: true
  //       }
  //     },
  //     defaultStyle: {
  //       columnGap: 20
  //     }
  //   };

  //   var savepdf = pdfMake.createPdf(docDefinition);
  //   pdfMake.createPdf(docDefinition).download();
  //   this.questionService.savePDF(savepdf).subscribe(
  //     (res) => {
  //       this.router.navigate(['/dashboard']);
  //       this.localStorageClear();
  //     });
  // }


  //         Apply  templates for pdf creation
  //========================================================//
  // pdfTemplate() {

  //   // default structure for all type of template 
  //   this.content = [];
  //   this.content.push(
  //     {
  //       text: this.testDetails.oName,
  //       style: 'header',
  //       alignment: 'center'
  //     },
  //     {
  //       text: this.testDetails.acaYear,
  //       alignment: 'center',
  //       bold: false
  //     },
  //     {
  //       text: `Class:  ${this.testDetails.class}`,
  //       alignment: 'center',
  //       bold: false
  //     },
  //     {
  //       columns: [
  //         {
  //           width: 'auto',
  //           text: `Duration:  ${this.testDetails.examTime} Minutes`
  //         },
  //         {
  //           width: '*',
  //           text: `Subject:  ${this.testDetails.subject} \n\n\n`,
  //           alignment: 'center',
  //           bold: false
  //         },
  //         {
  //           width: 'auto',
  //           text: `Total Marks:  ${this.testDetails.totalMarks} \n\n\n`
  //         }
  //       ]
  //     }
  //   );

  //   var i = 1;
  //   this.questions.forEach((question) => {

  //     let ques = {
  //       alignment: 'justify',
  //       columns: [
  //         {
  //           text: i + '.  ' + question.text + '\n' + '\n'
  //         }
  //       ]
  //     };
  //     this.content.push(ques);

  //     if (question.ques_type === 'mcq') {

  //       let options = {
  //         columns: [
  //           {
  //             width: '*',
  //             text: 'a)  ' + question.options[0]
  //           },
  //           {
  //             width: '*',
  //             text: 'b)  ' + question.options[1]
  //           },
  //           {
  //             width: '*',
  //             text: 'c)  ' + question.options[2]
  //           },
  //           {
  //             width: '*',
  //             text:
  //               'd)  ' +
  //               question.options[3] +
  //               '\n' +
  //               '\n' +
  //               '\n'
  //           }
  //         ]
  //       };
  //       this.content.push(options);

  //     }
  //     i++;
  //   });

  //   return this.content;
  // }

  checkEmail(email) {
    const regExp = /(^[a-z]([a-z_\.]*)@([a-z_\.]*)([.][a-z]{3})$)|(^[a-z]([a-z_\.]*)@([a-z_\.]*)(\.[a-z]{3})(\.[a-z]{2})*$)/i;
    return regExp.test(email);
  }

  checkEmails() {
    let flag = true;
    const emailArray = this.emailData.emails.split(',');
    for (let i = 0; i <= (emailArray.length - 1); i++) {
      if (emailArray[i][0] === ' ') {
        console.log('space');

        emailArray[i] = emailArray[i].slice(1, emailArray[i].length);
        console.log(emailArray[i]);
      }

      if (this.checkEmail(emailArray[i])) {
        console.log(emailArray[i]);
        // Do what ever with the email.
      } else {
        console.log(emailArray[i]);
        this.snackbar.openSnackBar('invalid Email :' + emailArray[i],'snackbar-failure');
        // this.ngFlashMessageService.showFlashMessage({
        //   messages: ['invalid Email :' + emailArray[i]],
        //   // dismissible: true, 
        //   timeout: 2000,
        //   type: 'danger'
        // });
        // alert("invalid email: " + emailArray[i]);
        flag = false;
      }
    }
    return flag;
  }

  async onEmailSubmit() {
    console.log(this.arr);
    console.log(this.emailData);
    if (this.checkEmails()) {
      console.log('valid');
    } else {
      console.log('invalid');

    }
    this.enterEmailShow = false;
    //  var emailArray = this.emails.split(',');
    //  console.log(emailArray);
    console.log(this.pdfLoader)
    var heightLeft = 841.89;
    const doc = new jspdf('p', 'pt', 'a4');
    const options = {
      pagesplit: true
    };
    var position = 0;

    // // const ids = document.querySelectorAll('[id]');
    const length = this.questions.length;
    // const id = document.getElementById("html-2-pdfwrapper");
    // doc.addHTML(id,0,0,options,function(){
    //   doc.save();
    // })
    var details = document.getElementById('test-details');
    var detailsHeight = document.getElementById('test-details').clientHeight;
    detailsHeight = (detailsHeight*72)/96
    heightLeft -= detailsHeight ;
    position = detailsHeight;

    html2canvas(details).then(function(canvas){
      doc.addImage(canvas.toDataURL('image/png'),'JPEG',0,0);
    })

    for (let i = 0; i < length; i++) {
      var chart = document.getElementById(JSON.stringify(i));
      var height = document.getElementById(JSON.stringify(i)).clientHeight;
      console.log("dsjdn");
      
      height = (height*72)/96;
      if(height>heightLeft){
        console.log("new page");
        heightLeft = 822;  
        position = 20;
        doc.addPage();   
      } 
        // excute this function then exit loop
        await html2canvas(chart, { scale: 1 }).then(function (canvas) { 
          doc.addImage(canvas.toDataURL('image/png'), 'JPEG',10,position );
          console.log(heightLeft);
          console.log(position);
          heightLeft -= height;
          position += height+10;
          heightLeft -= 10;
        });


    }
    // download the pdf with all charts
    console.log(doc);
    // console.log(doc.output('datauristring'));
    // doc.save(this.testDetails.testName + '.pdf');

    let data;
    const promise = new Promise((resolve, reject) => {
        data = doc.output('datauristring');
        console.log(data);
        resolve();
    }).then(() => {
      data = (data.substr(28));
      this.questionService.sendEmail(data, this.emailData);
    });
  }

  onClose() {
    this.enterEmailShow = false;
  }



  // -------------------------- Some other functions---------------------------------------------------

  // ############################################
  //     Generating image pdf through pdfmake
  // ############################################
  // pdfImagesLoop() {
  //   let i = 1;
  //   this.questions.forEach((question) => {
  //     this.imageArray.push(
  //       [{ text: question.text, margin: [10, 30] },
  //       {
  //         stack: [
  //           {
  //             image: question.img,
  //             width: 150
  //           }
  //         ]
  //       }]
  //     )
  //     i++;
  //   })
  //   this.arr.push({
  //     style: 'tableExample',
  //     table: {
  //       widths: ['auto', '*'],
  //       body: this.imageArray
  //     },
  //     layout: 'noBorders'
  //   })
  // }

  // ###################################
  //     Generating Pdf using jsPDF
  // ###################################
  // GeneratePDF() {
  //   html2canvas(document.getElementById('html-2-pdfwrapper')).then(function (canvas: HTMLCanvasElement) {
  //     // var pdf = new jspdf('p','pt','a4');
  //     // let options = {
  //     //        pagesplit: true
  //     //     };
  //     var imgData = canvas.toDataURL("image/png");
  //     // pdf.addHTML(canvas,0,0,options,function(){
  //     // pdf.addPage();
  //     // })
  //     // pdf.save('web.pdf');
  //     var imgWidth = 210;
  //     var pageHeight = 295;
  //     var imgHeight = canvas.height * imgWidth / canvas.width;
  //     var heightLeft = imgHeight;
  //     var doc = new jspdf('p', 'mm');
  //     var position = 0;

  //     doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
  //     heightLeft -= pageHeight;

  //     // pdfcreate.nativeElement.insertAdjacentElement('afterend','<br><br>');
  //     while (heightLeft >= 0) {
  //       doc.addPage();
  //       // document.getElementById('html-2-pdfwrapper').insertAdjacentElement('afterend','<br><br>')
  //       position = heightLeft - imgHeight;
  //       doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
  //       heightLeft -= pageHeight;
  //     }
  //     doc.save('file.pdf');
  //   });
  // }


  // // ###################################
  // //     Make pdf using jsPDF
  // // ###################################
  // makePDF() {

  //   var quotes = document.getElementById('test');

  //   html2canvas(quotes).then(function (canvas) {

  //     //! MAKE YOUR PDF
  //     var pdf = new jspdf('p', 'pt', 'letter');

  //     for (var i = 0; i <= quotes.clientHeight / 980; i++) {
  //       //! This is all just html2canvas stuff
  //       var srcImg = canvas;
  //       var sX = 0;
  //       var sY = 980 * i; // start 980 pixels down for every new page
  //       var sWidth = 900;
  //       var sHeight = 980;
  //       var dX = 0;
  //       var dY = 0;
  //       var dWidth = 900;
  //       var dHeight = 980;

  //       let onePageCanvas = document.createElement("canvas");
  //       onePageCanvas.setAttribute('width', '900');
  //       onePageCanvas.setAttribute('height', '980');
  //       var ctx = onePageCanvas.getContext('2d');
  //       // details on this usage of this function:
  //       // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
  //       ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

  //       // document.body.appendChild(canvas);
  //       var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

  //       var width = onePageCanvas.width;
  //       var height = onePageCanvas.clientHeight;

  //       //! If we're on anything other than the first page,
  //       // add another page
  //       if (i > 0) {
  //         pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
  //       }
  //       //! now we declare that we're working on that page
  //       pdf.setPage(i + 1);
  //       //! now we add content to that page!
  //       pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .62), (height * .62));

  //     }
  //     //! after the for loop is finished running, we save the pdf.
  //     pdf.save('Test.pdf');
  //   }
  //   )
  // }


  // makepdf111() {
  //   var quotes = document.getElementById('html-2-pdfwrapper');
  //   // html2canvas(quotes).then(function(canvas) {
  //   // console.log("djfd");

  //   // })
  //   quotes.setAttribute('width', '210mm')
  //   var pdf = new jspdf('p', 'pt', 'a4');
  //   var options = {
  //     pagesplit: true,
  //   };
  //   pdf.addHTML(quotes, options, function () {
  //     pdf.save("abc.pdf");
  //   })
  // }

  // downloadPdf() {
  //   let doc = new jspdf();

  //   let specialElementHandlers = {
  //     "#html-2-pdfwrapper": function (element, renderer) {
  //       return true;
  //     }
  //   };
  //   setTimeout(() => {
  //     let content = this.pdfcreate.nativeElement;
  //     console.log("content.innerHTML:-", content.innerHTML);

  //     html2canvas(content).then(canvas => {
  //       document.body.appendChild(canvas);
  //       var img = canvas.toDataURL();
  //       console.log("canvas:---", img);

  //       doc.addImage(img, "JPEG", 10, 40, 250, 200);
  //       doc.save("MY_QUESTION_BANK.pdf");
  //     });
  //   });
  // }
  
}


