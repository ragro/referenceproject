import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, BrowserXhr } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';
import { AppComponent } from './app.component';
import { QuestionBankComponent } from './question-bank/question-bank.component';
import { HtmlPdfComponent } from './html-pdf/html-pdf.component';
import { QuestionsService } from './services/questions.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthService } from './services/auth.service';
import { TestDetailComponent } from './test-detail/test-detail.component';
import { NgDragDropModule } from 'ng-drag-drop';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { HeaderComponent } from './header/header.component';
import { AuthGuard } from './services/auth-guard.service';
// import { NgFlashMessagesModule } from 'ng--messages';
import { ArrayUtils } from './utils/array-utils';
import {HeaderUtils} from './utils/headerUtils';
import { SnackBar } from './utils/snackbar';

import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatSnackBarModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatListModule} from '@angular/material/list';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSidenavModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

import { LogOutPopUpComponent } from './utils/log-out-pop-up/log-out-pop-up.component';
import { EmailPopUpComponent } from './utils/email-pop-up/email-pop-up.component';
import { HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { Constants } from './utils/constants';
import { ErrorMessageUtils } from './utils/errorMessageUtils';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'questionbank', component: QuestionBankComponent, canActivate: [AuthGuard] },
  { path: 'pdfview', component: HtmlPdfComponent, canActivate: [AuthGuard] },
  { path: 'testDetails', component: TestDetailComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: '**', component: PagenotfoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    QuestionBankComponent,
    HtmlPdfComponent,
    LoginComponent,
    RegisterComponent,
    TestDetailComponent,
    DashboardComponent,
    PagenotfoundComponent,
    HeaderComponent,
    LogOutPopUpComponent,
    EmailPopUpComponent,
    // SnackbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatCheckboxModule,
    MatListModule,
    MatPaginatorModule,
    MatSidenavModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    NgDragDropModule.forRoot(),
    NgProgressModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatTableModule,
    MatCheckboxModule,
    MatListModule,
    MatPaginatorModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatSnackBarModule
  ],
  entryComponents:[TestDetailComponent,DashboardComponent,LogOutPopUpComponent,EmailPopUpComponent],
  providers: [QuestionsService, AuthService, AuthGuard,  { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }, ArrayUtils , HeaderUtils,SnackBar],
  bootstrap: [AppComponent]
})
export class AppModule { }
