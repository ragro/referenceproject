import { QuestionsService } from '../services/questions.service';
import { Response } from '@angular/http';
import { Route, Router } from '@angular/router';
import { NgModule, ViewChild } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';
import { AuthService } from '../services/auth.service';
import { NgDragDropModule } from 'ng-drag-drop';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { Component, OnInit } from '@angular/core';
import {  trigger, state, style, transition, animate } from '@angular/animations';
import { ArrayUtils } from '../utils/array-utils';
import { MatDialog, MatSidenav } from '../../../node_modules/@angular/material';
import { TestDetailComponent } from '../test-detail/test-detail.component';
import { LogOutPopUpComponent } from '../utils/log-out-pop-up/log-out-pop-up.component';
import { Constants } from '../utils/constants';

import { SnackBar } from '../utils/snackbar';
import { map } from 'rxjs-compat/operator/map';
// import { SnackbarComponent } from '../utils/snackbar/snackbar.component';


@Component({
  selector: 'app-question-bank',
  templateUrl: './question-bank.component.html',
  styleUrls: ['./question-bank.component.css'],
  animations: [
    trigger('abc', [
      state('state1', style({
        width: '0px',
        height: '100%'
      })),
      state('state2', style({
        width: '250px',
        height: '100%'
      })),
      transition('state1=>state2', animate('150ms ease-out')),
      transition('state2=>state1', animate('150ms ease-out'))
    ])
  ]
})
export class QuestionBankComponent implements OnInit {

  constructor(public questionService: QuestionsService,
    public router: Router,
    private authService: AuthService,
    private progressService: NgProgress,
    private snackbar:SnackBar,
    // private ngFlashMessageService: NgFlashMessageService,
    private arrayUtils: ArrayUtils,
    private dialog: MatDialog
  ) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }
  questions: any[] = [];
  count1 = 0;
  count2 = 0;
  subjectCategory: any[];
  selectedQuestions = [];
  searchArray: any = [];
  search = '';
  subject: string;
  classes = true;
  state = 'state1';
  addButtonFlag = false;
  removeButtonFlag = false;
  selectedQuestionCount = 0;
  unselectedQuestionCount = 0;
  display: Boolean = false;
  mouseLeaveEvent: Boolean = true;
  testName: string;
  oName: string;
  acaYear: string;
  studentClass: string;
  examTime: string;
  totalMarks: number;
  isReadonly = true;
  unSelectedQuestionsMap = new Map();
  selectedQuestionsMap = new Map();
  mapData=[];
  array=[];
  keyss=[];
  show = false;
  flag = false;
  searchingElement:boolean=false;

  questionTypes = [];

  //pagination variables
  currentPage = 1;
  pageNo = 1;
  totalPages: number;
  startTestInstance: number;
  endTestInstance: number;
  dummyArray: boolean[] = [];
  count: number;
  perPage: number;
  unSelectedArrayQuestionIndex:number;
  checkedArray = [];
  store=[];
  @ViewChild('sidenav') sidenav: MatSidenav;
  testDetails: any;
  test: any;
  reason = '';
  k=[];

  close(reason: string,typeIndex:number) {
    console.log(typeIndex)
    if(typeIndex>0){
      this.questionService.questionTypee=this.questionTypes[typeIndex];
      this.pageNo=1;
      this.categoryQuestion(this.subject,true);
    }
    if(typeIndex==0){
      this.questionService.questionTypee=undefined;
      this.pageNo=1;
      this.categoryQuestion(this.subject,true);
    }
    this.reason = reason;
    this.sidenav.close();
  }
  ngOnInit() {
    this.questionTypes[0]='ALL';
    this.questionService.getQuestionTypes().subscribe(
      ((response)=> {
        this.questionTypes.push(...response['questionTypes']);
        console.log("Question types",this.questionTypes);       
      })
    )

    this.testDetails = localStorage.getItem(Constants.TEST);
    this.test = JSON.parse(this.testDetails);
    this.subject = this.testDetails.subject;
    this.gettestDetailsFromLocalstorage(this.test);
    this.unSelectedArrayQuestionIndex = 1;
    this.markSelectedQuestionsCheckedAfterReload();

    this.questionService.testDetailsOnQuestionBankChange.subscribe(
      (testDetails) => {
        // tslint:disable-next-line:no-shadowed-variable
        const promise = new Promise((resolve, reject) => {
          this.gettestDetailsFromLocalstorage(testDetails);
          resolve();
        }).then(() => {
          this.snackbar.openSnackBar('Changes saved successfully','snackbar-success');
          // this.ngFlashMessageService.showFlashMessage({
          //   messages: ['Changes successfully saved'],
          //   timeout: 2000,
          //   type: 'success'
          // });
        });
      }
    );
  }

  dashboard() {
    this.router.navigate(['/dashboard']);
  }

  // mark selected questions
  markSelectedQuestionsCheckedAfterReload() {
    // tslint:disable-next-line:no-shadowed-variable
    const promise1 = new Promise((resolve, reject) => {
      if (localStorage.getItem(Constants.SELECTED_QUESTIONS) !== undefined
        && localStorage.getItem(Constants.SELECTED_QUESTIONS) !== null) {
        this.selectedQuestions = JSON.parse(localStorage.getItem(Constants.SELECTED_QUESTIONS));
        // this.selectedQuestions=[];
        // console.log(this.selectedQuestions = JSON.parse(localStorage.getItem(Constants.SELECTED_QUESTIONS)))
        // tslint:disable-next-line:radix
        this.selectedQuestionCount = parseInt(localStorage.getItem(Constants.SELECTED_QUESTIONS));
      }
      resolve();
    }).then(() => {
        this.categoryQuestion(this.subject,true);      
    }).then(() => {
      if (localStorage.getItem(Constants.CHECKED_ARRAY) !== undefined && localStorage.getItem(Constants.CHECKED_ARRAY) !== null) {
        this.checkedArray = JSON.parse(localStorage.getItem(Constants.CHECKED_ARRAY));
        this.checkedArray.forEach((checkedElement) => {
          this.questions[checkedElement].checked = true;
        });
      }
    });
  }

  // Select Questions
  selectedQuestion(index: number) {
    let foundQuestionarrayIndex=this.questions.indexOf(this.searchArray[index]);
    console.log(foundQuestionarrayIndex);
    this.questions[foundQuestionarrayIndex].checked = !this.questions[foundQuestionarrayIndex].checked;
    if (this.questions[foundQuestionarrayIndex].checked) {
      this.checkedArray.push(foundQuestionarrayIndex);
      this.count1++;
    } else {
      this.checkedArray = this.arrayUtils.arrayFilter(this.checkedArray, foundQuestionarrayIndex);
      this.count1--;
    }
    localStorage.setItem(Constants.CHECKED_ARRAY, JSON.stringify(this.checkedArray));
    if (this.count1 > 0) {
      this.addButtonFlag = true;
    } else {
      this.addButtonFlag = false;
    }
  }

  selectAllQuestions() {
    this.checkedArray = [];
    for (let i = 0; i < this.questions.length; i++) {
      this.checkedArray.push(i);
      this.questions[i].checked = true;
      this.selectedQuestions.push(this.questions[i]);
    }
  }


  //   Deselect Questions
  deselectQuestion(index: number) {
    this.selectedQuestions[index].checked = !this.selectedQuestions[index].checked;
    if (this.selectedQuestions[index].checked) {
      this.count2++;
    } else {
      this.count2--;
    }
    if (this.count2 > 0) {
      this.removeButtonFlag = true;
    } else {
      this.removeButtonFlag = false;
    }
  }

  deselectAllQuestions() {
    this.checkedArray = [];
    for (let i = 0; i < this.questions.length; i++) {
      this.questions[i].checked = false;
    }
  }


  //   Shift Questions
  selected() {
    this.addButtonFlag = false;  
    for (let j = 0; j < this.questions.length; j++) {
      if (this.questions[j].checked === true) {
        this.selectedQuestions.push(this.questions[j]);
        if(this.selectedQuestionsMap.has(this.pageNo)){
          this.mapData=this.selectedQuestionsMap.get(this.pageNo)
          this.mapData.push(this.questions[j]);
          this.selectedQuestionsMap.set(this.pageNo,this.mapData);
        }else{
          this.mapData.push(this.questions[j]);
          this.selectedQuestionsMap.set(this.pageNo,this.mapData);
        }
        this.questions[j].checked = false;
        this.questions.splice(j,1)
        j--;
        this.keyss = Array.from( this.selectedQuestionsMap.keys());
        localStorage.setItem('keys',JSON.stringify(this.keyss));
        localStorage.setItem(JSON.stringify(this.pageNo),JSON.stringify(this.selectedQuestionsMap.get(this.pageNo)));  
        console.log('mapdate',this.mapData);     
        this.mapData=[];
      }
    }
    this.unSelectedQuestionsMap.set(this.pageNo,this.questions);
    console.log("working",this.selectedQuestionsMap);
    // this.searchingElement=false;
    // this.search='';
    this.checkedArray = [];
    localStorage.removeItem(Constants.CHECKED_ARRAY);
    this.searchArray = this.unSelectedQuestionsMap.get(this.pageNo);
    console.log("Mappp")
    this.syncFunction();
    this.count1 = 0;
    this.countQuestions();
  }

  //  Remove Questions
  remove() {
    console.log('rem'+this.pageNo);
    this.removeButtonFlag = false;
    for (let j = 0; j < this.selectedQuestions.length; j++) {
      if (this.selectedQuestions[j].checked === true) {
        console.log(this.selectedQuestions[j]._id);
        console.log(this.selectedQuestionsMap.entries());  
        this.removingQuestion(j);  
        this.selectedQuestions[j].checked = false;
        this.selectedQuestions.splice(j, 1);
        j--;
      }
    }
    this.searchArray=this.unSelectedQuestionsMap.get(this.pageNo);
    this.questions=this.searchArray;
    // this.searchArray = this.questions;
    // this.countQuestions();
    // this.localstorageSetQuestions();
    this.syncFunction();
    this.count2 = 0;

    this.countQuestions();
  }


  //  Search Questions
  searchQuestion() {
    let keys= Array.from(this.unSelectedQuestionsMap.keys());
    let allQuestionArray=[];
    console.log(keys);
    if(this.search !== ''){
       this.searchingElement=true;  
       console.log(this.searchingElement);
       for(let i=0;i<keys.length;i++){
               allQuestionArray.push(...this.unSelectedQuestionsMap.get(keys[i]));
              }
              this.searchArray=allQuestionArray.filter((q)=>{
                return q.text.toLowerCase().indexOf(this.search.toLowerCase()) > -1;           
              })     
              console.log(this.searchArray.length+'aaaaaa');
              if(this.searchArray!=''){
                this.store=this.searchArray;
                this.questions=this.searchArray;
                this.unSelectedArrayQuestionIndex=1;  
                this.unselectedQuestionCount=this.searchArray.length;
                this.pageNo=1;
                this.totalPages=Math.ceil(this.searchArray.length/20);  
                this.pageNoManipulation();  
                this.onPageno(this.pageNo);
              }else{
                this.unselectedQuestionCount=this.searchArray.length;
                this.totalPages=0;
                this.pageNoManipulation();
              }

      }
    else{
      this.searchingElement=false;
      console.log(this.searchingElement);
      this.pageNo=1;
      this.categoryQuestion(this.subject,true);
    }  
  }


  //  Convert PDF
  convertToPdf() {
    this.questionService.passToPDF(this.selectedQuestions, this.questions);
    this.router.navigate(['/pdfview']);
  }


  //  Question Category
  categoryQuestion(category,shouldReload) {
    this.closeNav();
    this.subject = category;
    // if(this.unSelectedQuestionsMap.has(this.pageNo)&&!shouldReload){
    //     this.questions=this.unSelectedQuestionsMap.get(this.pageNo);
    //     this.searchArray=this.questions;
    // }
    // else{  
    this.questionService.categoryQuestions('', category, this.pageNo).subscribe(
      (res) => {
        if(localStorage.getItem(Constants.SELECTED_QUESTIONS)){
          this.selectedQuestions = JSON.parse(localStorage.getItem(Constants.SELECTED_QUESTIONS));
        }else{
          this.selectedQuestions=[];
        }
        this.questions = res[Constants.QUESTIONS];
        for(let i=0;i<this.selectedQuestions.length;i++){
          for(let j=0;j<res[Constants.QUESTIONS].length;j++){
             if(this.selectedQuestions[i]._id == res[Constants.QUESTIONS][j]._id){
               this.questions.splice(j,1);
               break;
             }
          }
        }
        this.unSelectedQuestionsMap.set(this.pageNo,this.questions);
        console.log(this.unSelectedQuestionsMap)
        console.log("search array",this.unSelectedQuestionsMap.get(this.pageNo));
        this.searchArray = this.questions;
        this.totalPages = res['pages'];
        this.dummyArray = new Array(this.totalPages);
        this.count = res['count'];
        this.perPage = res['perPage'];
        if (this.count) {
          this.startTestInstance = ((this.pageNo - 1) * this.perPage) + 1;
        } else {
          this.startTestInstance = 0;
        }
        this.endTestInstance = this.perPage * this.pageNo;

        if (this.endTestInstance > this.count) {
          this.endTestInstance = this.count;
        }

        this.selectedQuestions.forEach((selectedQuestion) => {
          this.searchArray = this.arrayUtils.arrayObjectFilter(this.questions, selectedQuestion);
          // this.unSelectedArrayQuestionIndex = this.unSelectedArrayQuestionIndex - (this.perPage - this.searchArray.length);
          this.questions = this.searchArray;
        });
        this.unSelectedArrayQuestionIndex = (this.perPage * (this.pageNo - 1)) + 1;
        this.pageNoManipulation();
        this.syncFunction();
        this.findingImageUrl();
        localStorage.setItem(Constants.QUESTIONS, JSON.stringify(res[Constants.QUESTIONS]));
      }
    );
  // }
  }


  //  Logout
  logout() {
    this.show = true;
    this.authService.logOutPopUp = this.dialog.open(LogOutPopUpComponent);
    this.authService.logOutPopUp.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  // Logout confirmation
  onYes() {
    this.authService.logout();
    localStorage.removeItem(Constants.TEST);
    localStorage.removeItem(Constants.USER);
    this.router.navigate(['/']);
    return false;
  }

  onNo() {
    this.show = false;
  }


  //  Side Navbar click
  navClick() {
    this.classes = !this.classes;
    this.openNav();
  }


  //  Drag & Drop questions
  onItemDrop(e: any) {
    if (this.count1 > 0 && this.questions[e.dragData.index].checked) {
      this.count1--;
    }
    if (this.count1 === 0) {
      this.addButtonFlag = false;
    }
    if(this.selectedQuestionsMap.has(this.pageNo)){
      this.mapData=this.selectedQuestionsMap.get(this.pageNo)
      this.mapData.push(e.dragData.ques);
      this.selectedQuestionsMap.set(this.pageNo,this.mapData);
    }else{
      this.mapData.push(e.dragData.ques);
      this.selectedQuestionsMap.set(this.pageNo,this.mapData);
    }
    this.questions[e.dragData.index].checked = false;
    this.questions.splice(e.dragData.index,1);
    this.keyss = Array.from( this.selectedQuestionsMap.keys());
    localStorage.setItem('keys',JSON.stringify(this.keyss));
    localStorage.setItem(JSON.stringify(this.pageNo),JSON.stringify(this.selectedQuestionsMap.get(this.pageNo)));  
    console.log('mapdate',this.mapData);     
    this.mapData=[];
    this.unSelectedQuestionsMap.set(this.pageNo,this.questions);
    this.searchingElement=false;
    // this.search='';
    this.checkedArray = this.arrayUtils.arrayFilter(this.checkedArray, e.dragData.index);
    this.selectedQuestions.push(e.dragData.ques);
    this.searchArray = this.unSelectedQuestionsMap.get(this.pageNo);
    this.syncFunction();
  }


  //  Drag & Drop selected questions
  onItemReturn(e: any) {
    let keys , getSelectedQuestionMap;
    if (this.count2 > 0 && this.selectedQuestions[e.dragData.index].checked) {
      this.count2--;
    }
    if (this.count2 === 0) {
      this.removeButtonFlag = false;
    }
    this.removingQuestion(e.dragData.index);

    this.selectedQuestions[e.dragData.index].checked = false;
    this.selectedQuestions.splice(e.dragData.index, 1);
    this.questions=this.unSelectedQuestionsMap.get(this.pageNo);    
    this.searchArray = this.questions;
    this.syncFunction();

  }


  //  Open side navbar
  openNav() {
    this.state = 'state2';
  }


  //   Close side navbar
  closeNav() {
    this.state = 'state1';
  }



  //   Close side navbar event
  navclose(e: any) {
    if (!e.target.closest('.navabc')) {
      this.closeNav();
    }
  }


  // count questions
  countQuestions() {
    this.selectedQuestionCount = this.selectedQuestions.length;
    this.unselectedQuestionCount = this.questions.length;
  }


  // set questions in local stoarge
  localstorageSetQuestions() {
    localStorage.setItem(Constants.UNSELECTED_QUESTIONS_COUNT, String(this.unselectedQuestionCount));
    localStorage.setItem(Constants.SELECTED_QUESTIONS_COUNT, String(this.selectedQuestionCount));
    localStorage.setItem(Constants.QUESTIONS, JSON.stringify(this.questions));
    localStorage.setItem(Constants.SELECTED_QUESTIONS, JSON.stringify(this.selectedQuestions));
    // localStorage.setItem(Constants.SELECTED_QUESTIONS, JSON.stringify(this.selectedQuestionsMap));    
  }

  MCQQuestions() {
    this.categoryQuestion(this.subject,false);
    localStorage.removeItem(Constants.SELECTED_QUESTIONS);
    this.selectedQuestions = [];
    this.questionService.questionType = 'mcq';
    this.closeNav();
  }

  ImageQuestions() {
    this.searchArray = this.questionService.imgQuestions;
    localStorage.removeItem(Constants.SELECTED_QUESTIONS);
    this.selectedQuestions = [];
    this.questions = this.questionService.imgQuestions;
    this.questionService.questionType = 'image';
    this.closeNav();
  }

  gettestDetailsFromLocalstorage(testDetail) {
    const acaYear1 = testDetail.startYear + '-' + testDetail.endYear;
    this.subject = testDetail.subject;
    this.testName = testDetail.testName;
    this.oName = testDetail.oName;
    this.acaYear = acaYear1;
    this.studentClass = testDetail.class;
    this.examTime = testDetail.examTime;
    this.totalMarks = testDetail.totalMarks;
  }

  editTestDetails() {
    this.questionService.testDetailDialogBox = this.dialog.open(TestDetailComponent, { width: '900px' });
    this.questionService.show = true;
    this.flag = true;
    this.questionService.editMode = true;
  }

  closeTestDetails(e: any) {
    if (this.flag) {
      this.flag = false;
    } else {
      if (e.target.closest('.modal-dialog.cascading-modal')) {

      } else {
        this.questionService.show = false;
        this.flag = false;

      }

    }
  }

  pageNoManipulation() {
    var pageArray = [];
    var pageShow = 5;
    var x = 0, y, z = 0;

    var startPage = this.pageNo - Math.floor(pageShow / 2);
    var endPage: number = Math.floor(pageShow / 2) + this.pageNo;
    y = startPage;
    if (this.totalPages < pageShow) {
      for (y = 1; y <= this.totalPages; y++) {
        pageArray.push(y);
      }
      this.dummyArray = pageArray;
    }
    else {
      if (startPage <= 0) {
        x = 1 - startPage;
      }
      if (endPage > this.totalPages) {
        z = endPage - this.totalPages;
        startPage = startPage - z;
      }
      for (y = startPage + x; y < startPage + 5 + x; y++) {
        pageArray.push(y);
      }
      this.dummyArray = pageArray;
    }
  }

  onNext() {
    if (this.pageNo < this.totalPages) {
      this.pageNo++;
      this.questionService.questionTypee=undefined;
      this.unSelectedArrayQuestionIndex = (this.perPage * (this.pageNo - 1)) + 1;
      this.categoryQuestion(this.subject,false);
      // this.router.navigate(['/admin/test-instances/'+this.pageNo]);
    }
  }

  onPrev() {
    if (this.pageNo > 1) {
      this.pageNo--;
      this.unSelectedArrayQuestionIndex = (this.perPage * (this.pageNo - 1)) + 1;
      this.questionService.questionTypee=undefined;
      this.categoryQuestion(this.subject,false);
      // this.router.navigate(['/admin/test-instances/'+this.pageNo]);
    }
  }

  onPageno(i) {

    this.pageNo = i;
    console.log('page'+this.pageNo);
    localStorage.setItem('pageNo',JSON.stringify(this.pageNo));
    let local=[];
    this.searchArray=this.store;
    if(this.searchingElement && this.searchArray!=''){
      console.log('pagina');
      console.log((this.perPage * (this.pageNo - 1)) + 1);
      let startIndex=(this.perPage * (this.pageNo - 1));
      for(i=0;i<20;i++){
          local[i]=this.searchArray[startIndex];
          if(this.searchArray[startIndex+1]==undefined || this.searchArray[startIndex+1] == '') break;
          startIndex++;
      }
      this.unSelectedQuestionsMap.set(this.pageNo,local);
      this.searchArray=local;
      this.questions=this.searchArray;
    }else{
      this.categoryQuestion(this.subject,false);
      if(this.unSelectedQuestionsMap.has(this.pageNo)){
        let arrayLength=this.unSelectedQuestionsMap.get(this.pageNo);
        this.questionService.questionTypee=undefined;
        console.log(arrayLength.length)
        this.unselectedQuestionCount = arrayLength.length;
      }
    }
    this.countQuestions();
    this.unSelectedArrayQuestionIndex = (this.perPage * (this.pageNo - 1)) + 1;
    // this.router.navigate(['/admin/test-instances/'+this.pageNo]);

  }

  syncFunction() {
    // tslint:disable-next-line:no-shadowed-variable
    const promise = new Promise((resolve, reject) => {
      this.countQuestions();
      resolve();
    }).then(() => {
      this.localstorageSetQuestions();
    });
  }




  convertImgToBase64(url, callback) {
    let canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image;
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
      canvas.height = img.height;
      canvas.width = img.width;
      ctx.drawImage(img, 0, 0);
      const dataURL = canvas.toDataURL('image/png');
      callback.call(this, dataURL);
      // Clean up
      canvas = null;
    };
    // img.src = "http://upload.wikimedia.org/wikipedia/commons/4/4a/Logo_2013_Google.png";
    img.src = url;

  }
  findingImageUrl() {
    console.log('questions', this.searchArray);
    // tslint:disable-next-line:no-shadowed-variable
    for (let element = 0; element < this.searchArray.length; element++) {
      const startingPoint = this.searchArray[element].text.indexOf('<img');
      const endPoint = this.searchArray[element].text.indexOf('>', startingPoint);
      const srcStartingPoint = this.searchArray[element].text.indexOf('src=', startingPoint);
      // let srcEndPoint=this.questions[element].text.indexOf("\"",srcStartingPoint);
      const imageTag = this.searchArray[element].text.slice(startingPoint, endPoint + 1);
      const imageURL = this.searchArray[element].text.slice((srcStartingPoint + 5), (endPoint - 3));

      const currentElement = this.searchArray[element];
      if (imageURL.search('http') !== -1 || imageURL.search('https') !== -1) {
        this.convertImgToBase64(imageURL, function (base64image) {
          //  console.log(element);
          currentElement.text = currentElement.text.replace(imageURL, base64image);
          //  console.log(currentElement.text);
        });
      }
      this.searchArray[element] = currentElement;
    }
  }

 removingQuestion(index){
   let keys,getSelectedQuestionMap;
   console.log('rem1'+this.pageNo);
  if(localStorage.getItem('keys')){
    keys=JSON.parse(localStorage.getItem('keys'))  
  }else{
    keys = Array.from( this.selectedQuestionsMap.keys());
    localStorage.setItem('keys',JSON.stringify(keys))
  }
  console.log(keys);
  for(let i=0;i<keys.length;i++){
    console.log(keys[i]);
    if(this.selectedQuestionsMap.get(keys[i])){
      console.log('check for key',this.selectedQuestionsMap.get(keys[i]))
      getSelectedQuestionMap=this.selectedQuestionsMap.get(keys[i]);
    }else{
      getSelectedQuestionMap=JSON.parse(localStorage.getItem(JSON.stringify(keys[i])));
    }
    console.log(getSelectedQuestionMap)
    console.log(this.selectedQuestions[index]._id)
    for(let k=0;k<getSelectedQuestionMap.length;k++){
      if(getSelectedQuestionMap[k]._id===this.selectedQuestions[index]._id){
        console.log(this.unSelectedQuestionsMap.get(keys[i]));
        let getUnselectedQuestionMap=this.unSelectedQuestionsMap.get(keys[i]);
        if(getUnselectedQuestionMap!==undefined){
          getUnselectedQuestionMap.push(getSelectedQuestionMap[k]);
          this.unSelectedQuestionsMap.set(keys[i],getUnselectedQuestionMap);
          getSelectedQuestionMap.splice(k,1);
          this.selectedQuestionsMap.set(keys[i],getSelectedQuestionMap);
          localStorage.setItem(JSON.stringify(keys[i]),JSON.stringify(getSelectedQuestionMap));
          console.log("unselected",this.unSelectedQuestionsMap);
          this.questions=this.unSelectedQuestionsMap.get([i]);
          this.searchArray=this.questions;
          console.log('selected',this.selectedQuestionsMap)
        }else{
          getSelectedQuestionMap.splice(k,1);
          this.selectedQuestionsMap.set(keys[i],getSelectedQuestionMap);
          localStorage.setItem(JSON.stringify(keys[i]),JSON.stringify(getSelectedQuestionMap));
        }
      }
    }
  }
 }  


}
