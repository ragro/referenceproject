import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '../../../node_modules/@angular/common/http';
import { HeaderUtils } from '../utils/headerUtils';
import { Constants } from '../utils/constants';
import { ErrorMessageUtils } from '../utils/errorMessageUtils';

@Injectable()
export class AuthService {
  authToken: any;
  user: any;
  allowChild = false;
  apiUrl = environment.apiurl;
  logOutPopUp;
  emailPopUp;
  constructor(private http: HttpClient) { }


  //  Error Handling
  _errorHandler(error: Response) {
    if (error.status === 0) {
      return Observable.throw({ _body: JSON.stringify({ msg:ErrorMessageUtils.NOT_RESPONDING }) });
    } else if (error.status > 400 && error.status < 500) {
      return Observable.throw(error || { _body: JSON.stringify({ msg: ErrorMessageUtils.SOMETHING_WENT_WRONG}) });
    }
    return Observable.throw(error || ErrorMessageUtils.SERVER_ERROR);
  }


  //  Register User
  registerUser(user) {
    let headers = HeaderUtils.headers;  
    return this.http
      .post(this.apiUrl + 'users/register', user,{
        headers: headers
      })
      .catch(this._errorHandler);
  }

  // Authenticate User
  authenticateUser(user) {
    let headers = HeaderUtils.headers;  
    return this.http
      .post(this.apiUrl + 'users/authenticate', user, {
        headers: headers
      })
      .catch(this._errorHandler);
  }


  //  User in localstorage
  storeUserData(token, user) {
    localStorage.setItem(Constants.ID_TOKEN, token);
    localStorage.setItem(Constants.USER, JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }


  //  Authentication
  isAuthenticated(): boolean {
    const token = localStorage.getItem(Constants.ID_TOKEN);
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  //   Logout
  logout() {
    this.authToken = null;
    this.user = null;
    this.allowChild = false;
    localStorage.clear();
  }
}
