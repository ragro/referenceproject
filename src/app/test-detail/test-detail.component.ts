import { Component, OnInit, Input } from '@angular/core';
import { QuestionsService } from '../services/questions.service';
import { Router } from '@angular/router';
import { Constants } from '../utils/constants';


@Component({
  selector: 'app-test-detail',
  templateUrl: './test-detail.component.html',
  styleUrls: ['./test-detail.component.css']
})
export class TestDetailComponent implements OnInit {
  test: any;
  testName = '';
  oName = '';
  class = '';
  acaYear = '';
  subject = '';
  subName = '';
  subjectName = '';
  examTime: number = null;
  totalMarks: number = null;
  subjectCategories:any = [];
  testDetail: any;
  selectCategory:boolean=false;
  mode:boolean;
  minYear;
  maxYear;
  yearDummyArray = [];
  startYear;
  endYear;

  constructor(public questionService: QuestionsService,
    private router: Router) { }

  // OnInit
  ngOnInit() {
    if(localStorage.getItem(Constants.TEST)){
      this.mode=true;
    }
    else{
      this.mode=false;
    }
    if (this.subName != null && this.subName !== undefined) {
      console.log('cannot edit form');
    } else {
      console.log('edit form details');
    }

    const currentTime = new Date();
    const year = currentTime.getFullYear();
    this.minYear = year - 10;
    this.maxYear = year + 50;

    for (let i = this.minYear; i < this.maxYear; i++) {
      this.yearDummyArray.push(i);
    }

    if (localStorage.getItem(Constants.TEST) != null && localStorage.getItem(Constants.TEST) !== undefined) {
      this.testDetail = localStorage.getItem(Constants.TEST);
      const test = JSON.parse(this.testDetail);
      this.testName = test.testName;
      this.oName = test.oName;
      this.class = test.class;
      this.subject = test.subject;
      this.examTime = test.examTime;
      this.totalMarks = test.totalMarks;
      this.startYear = test.startYear;
      this.endYear = test.endYear;
    } else {
      this.startYear = this.minYear;
      this.endYear = this.minYear;
    }
    this.questionService.selectedQuestions = [];
    this.questionService.unselectedQuestions = [];
    this.questionService.subjectCategory().subscribe(
      (res) => {
        this.subjectCategories = res;
      }
    );
  }

  // choose subject from dropdown
  selectSubject(event) {
    const length = event.target.value.length;
    const value = event.target.value;
    this.subName = value.slice(3, length);
    // this.subject = value.slice(3, length);
    localStorage.setItem('subName', this.subName);
  }


  // On form submit
  onSubmit(f) {
    if (!f.valid || f.value.startYear > f.value.endYear) {
      console.log('Invalid Date Format');
    } else {
      // this.questionService.editMode = false;
      // console.log(this.questionService.editMode);
      // console.log(document.getElementById('selectCategory'));
      console.log(f.value);
      this.test = f.value;
      if(this.questionService.editMode){
        let subject = JSON.parse(localStorage.getItem(Constants.TEST)).subject;
        console.log(subject);
        this.test.subject = subject;
        console.log(this.test.subject);
        
      }
      console.log("fdhdghdsg");     
      console.log(this.test);
      localStorage.setItem(Constants.TEST, JSON.stringify(this.test));
      this.questionService.show = false;
      this.router.navigate(['/questionbank']);
      this.questionService.testDetailsOnQuestionBankChange.next(this.test);
      this.questionService.testDetailDialogBox.close();
    }
  }
  // selected(x) {
  //   document.form1.select.options[x].selected = true;
  //   if (x != 0) {
  //      document.form1.submitOK.value = false;
  //   }
  //   else {
  //      document.form1.submitOK.value = true;
  //   }
  // }
  checkSelectBox(){
    this.selectCategory=true;
    console.log(this.selectCategory);    
  }
   closeTestDetails(){
    this.questionService.testDetailDialogBox.close();
  }

}
