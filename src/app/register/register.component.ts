import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { NgModule } from '@angular/core';
// import { NgFlashMessageService } from 'ng-flash-messages';
import { FormGroup, Validators, FormBuilder } from '../../../node_modules/@angular/forms';
import { Constants } from '../utils/constants';
import { SnackBar } from '../utils/snackbar';
declare var alertify: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  credentialsForm: FormGroup;
  name = '';
  insName = '';
  email = '';
  password = '';
  confirmPassword = '';
  isSubmitted: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private route: Router,
    private snackbar: SnackBar
    ) {
    this.credentialsForm = this.formBuilder.group({
      name: [
        '', Validators.compose([
          Validators.pattern(/^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/),
          Validators.required
        ])
      ],
      email: [
        '', Validators.compose([
          Validators.pattern(Constants.EMAIL_VALIDATION),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.pattern(/^(?=^.{8,}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/),
          Validators.required
        ])
      ],
      confirmPassword: [
        '', Validators.compose([
          Validators.required,
          this.isValid
        ])
      ]
    });
  }

  ngOnInit() {
  }


  //   Register User
  onRegisterSubmit(validity) {
    this.isSubmitted = true;
    const user = {
      name: this.credentialsForm.value.name,
      email: this.credentialsForm.value.email,
      password: this.credentialsForm.value.password
    };
    const logginUser = {
      email: this.credentialsForm.value.email,
      password: this.credentialsForm.value.password
    };

    if (validity && this.confirmPassword === this.password) {
      this.authService.registerUser(user).subscribe(
        data => {
          // alertify.notify(data['msg'], 'success', 5);
          this.authService.authenticateUser(logginUser).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            data => {
              this.authService.storeUserData(data[Constants.TOKEN], data[Constants.USER]);
              localStorage.setItem('status', data[Constants.USER].status);
              this.route.navigate(['/dashboard']);
            },
            err => {
              const error = JSON.parse(err._body).msg;
              this.snackbar.openSnackBar(error,'snackbar-failure');
              // alertify.alert(error);
              // this.ngFlashMessageService.showFlashMessage({
              //   messages: [error],
              //   // dismissible: true, 
              //   timeout: 2000,
              //   type: 'danger'
              // });
            })


        }
      );
    }
  }

  // Navigate to signin page
  signIn() {
    this.route.navigate(['/']);
  }


  //  Checking the Validity of Password and ConfirmPassword
  isValid(form: FormGroup) {
    if (form.parent) {
      if (form.parent.controls['password'].value !== form.parent.controls['confirmPassword'].value) {
        return { 'Password don\'t match': true };
      }
    }
    return null;
  }

  // checking confirm password validity
  checkfield() {
    const allErr: any = {};
    const cpassRegCtl = this.credentialsForm.controls.confirmPassword;
    if (cpassRegCtl.invalid && (cpassRegCtl.touched || this.isSubmitted))
      if (cpassRegCtl.errors.required) 
        allErr.g = true;
      else 
        allErr.h = true;

    return allErr;
    
  }
}
