import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FormGroup, Validators, FormBuilder } from '../../../node_modules/@angular/forms';
import { HeaderUtils } from '../utils/headerUtils';
import { Constants } from '../utils/constants';
import { SnackBar } from '../utils/snackbar';
declare var alertify: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authenticationError = '';
  displaybutton = false;
  alertmsg = true;
  credentialsForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
    public route: Router,
    private authService: AuthService,
    private snackbar:SnackBar
    // private ngFlashMessageService: NgFlashMessageService
  ) {
    this.credentialsForm = this.formBuilder.group({
      email: [
        '', Validators.compose([
          Validators.pattern(Constants.EMAIL_VALIDATION),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.required
        ])
      ]
    });
  }

  ngOnInit() {
    console.log(HeaderUtils.headers.get('Content-Type'));
    console.log(HeaderUtils.headers);   
  }

  //   Navigate to registration Page
  register() {
    this.route.navigate(['/register']);
  }


  // Login Submit
  onLoginSubmit(validity) {
    this.authenticationError = '';
    const user = {
      email: this.credentialsForm.value.email,
      password: this.credentialsForm.value.password
    };

    if (validity) {
      this.authService.authenticateUser(user).subscribe(
        data => {
          if (data[Constants.EXPIRED]) {
            this.displaybutton = true;
            this.authenticationError = data[Constants.MESSAGE];
          } else {
              this.authService.storeUserData(data[Constants.TOKEN], data[Constants.USER]);
              this.snackbar.openSnackBar('You are logged in', 'snackbar-success')
              localStorage.setItem('status', data[Constants.USER].status);
              this.route.navigate(['/dashboard']);
          }
        },
        err => {
          this.snackbar.openSnackBar(err.error.msg,'snackbar-failure');
          console.log('error case', err.error);
        })
    }
  }
}

