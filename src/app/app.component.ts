import { Component, ViewChild, OnInit, AfterViewChecked, AfterViewInit } from '@angular/core';
import { MatSidenav } from '../../node_modules/@angular/material';
import { QuestionsService } from './services/questions.service';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, Event, NavigationStart } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit   {
  title = 'app';
  isHeaderComponent = false;

  constructor(private r:ActivatedRoute,private router:Router){
    this.router.events.subscribe((event: Event) => {

      if(event instanceof   NavigationStart){
        console.log(event.url); 
        if(event.url == '/questionbank' || event.url == '/' || event.url == '/register' ){
          console.log(event.url,"false");
          this.isHeaderComponent = false;
        } else {
          console.log(event.url,"true");
          
          this.isHeaderComponent = true;
        }
      }
    
      
  });
  }

  ngAfterViewInit(){
    // this.r.url.subscribe((url)=> {
    //   console.log(this.r.children[0]);
    //   console.log(this.r);
    //   console.log(this.r.snapshot);
      
    // })
//     console.log(this.router.events);
//     this.router.events.subscribe((event) => {
//       console.log(this.r.snapshot); 
// });
    // if(this.route.routeConfig.component.name == 'DashboardComponent'){
    //   this.isQuestionBankComponent = true;
    // }
  }
}
